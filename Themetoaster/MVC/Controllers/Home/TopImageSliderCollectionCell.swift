//
//  TopImageSliderCollectionCell.swift
//  Themetoaster
//
//  Created by Gabani King on 17/07/21.
//

import UIKit

class TopImageSliderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSlider: UIImageView!
}
