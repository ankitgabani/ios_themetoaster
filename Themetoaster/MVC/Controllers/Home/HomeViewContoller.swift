//
//  HomeViewContoller.swift
//  Themetoaster
//
//  Created by Gabani King on 16/07/21.
//

import UIKit

class HomeViewContoller: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewAllProduct: UIView!
    
    @IBOutlet weak var collectionViewTopImageSlider: UICollectionView!
    
    @IBOutlet weak var collectionViewMultipleSlider: UICollectionView!
    
    var arrTopImageSlider = ["01","02"]
    
    var pageMenuSlider = UIPageControl()
    
    let sectionInsetsSlider = UIEdgeInsets(top: 0.0,
                                           left: 0.0,
                                           bottom: 0.0,
                                           right: 0.0)
    
    var flowLayoutSlider: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider.left * (1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 80)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    var flowLayoutMultipleSlider: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsetsSlider.left * (1)
        let availableWidth = collectionViewMultipleSlider.frame.width - paddingSpace
        let widthPerItem = availableWidth / 1
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 135)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageMenuSlider.numberOfPages = arrTopImageSlider.count
        
        self.collectionViewTopImageSlider.collectionViewLayout = flowLayoutSlider
        collectionViewTopImageSlider.delegate = self
        collectionViewTopImageSlider.dataSource = self
        
        self.collectionViewMultipleSlider.collectionViewLayout = flowLayoutMultipleSlider
        collectionViewMultipleSlider.delegate = self
        collectionViewMultipleSlider.dataSource = self

        
        
        viewAllProduct.clipsToBounds = true
        viewAllProduct.layer.cornerRadius = 10
        viewAllProduct.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner] // Left Corner
        
        viewSearch.clipsToBounds = true
        viewSearch.layer.cornerRadius = 4
        viewSearch.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] // Right Corner
        
        startTimer()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewTopImageSlider
        {
            return arrTopImageSlider.count
        }
        else
        {
            return 4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewTopImageSlider
        {
            let cell = collectionViewTopImageSlider.dequeueReusableCell(withReuseIdentifier: "TopImageSliderCollectionCell", for: indexPath) as! TopImageSliderCollectionCell
            
            cell.imgSlider.image = UIImage(named: arrTopImageSlider[indexPath.row])
            
            return cell
        }
        else
        {
            let cell = collectionViewMultipleSlider.dequeueReusableCell(withReuseIdentifier: "TopMultipaleSliderCollectionCell", for: indexPath) as! TopMultipaleSliderCollectionCell
                        
            return cell
        }        
    }
    
    @IBAction func clickedBackMultipleSlider(_ sender: Any) {
        
        scrollToPreviousOrNextCell(direction: "Previous")
    }
    
    @IBAction func clickedNextMultipleSlider(_ sender: Any) {
        scrollToPreviousOrNextCell(direction: "Next")
    }
    
    @objc func scrollToNextCell(){
        
        if pageMenuSlider.currentPage == pageMenuSlider.numberOfPages - 1 {
            pageMenuSlider.currentPage = 0
        } else {
            pageMenuSlider.currentPage += 1
        }
        collectionViewTopImageSlider.scrollToItem(at: IndexPath(row: pageMenuSlider.currentPage, section: 0), at: .right, animated: true)
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }
    
    
    func scrollToPreviousOrNextCell(direction: String) {
        
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                
                let firstIndex = 0
                let lastIndex = 4 - 1
                
                let visibleIndices = self.collectionViewMultipleSlider.indexPathsForVisibleItems
                
                let nextIndex = visibleIndices[0].row + 1
                let previousIndex = visibleIndices[0].row - 1
                
                let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
                let previousIndexPath: IndexPath = IndexPath.init(item: previousIndex, section: 0)
                
                if direction == "Previous" {
                    if previousIndex < firstIndex {
                        
                    } else {
                        self.collectionViewMultipleSlider.scrollToItem(at: previousIndexPath, at: .centeredHorizontally, animated: true)
                    }
                } else if direction == "Next" {
                    
                    if nextIndex > lastIndex {
                        
                    } else {
                        
                        self.collectionViewMultipleSlider.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
                    }
                }
            }
        }
    }
}

